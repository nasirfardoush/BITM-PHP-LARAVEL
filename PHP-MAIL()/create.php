<!DOCTYPE html>
<html>
<head>
	<title>Contact me</title>
	<style>
		table{
			margin: 0px auto;
			background-color: #eef1ee;
			padding: 20px;
			min-width: 500px;
		}
		input{
			padding: 5px;
		}
	</style>
</head>
<body>
	<form action="sendMail.php" method="POST">
		<table>
			<th colspan="2">
				<h2>CONTACT<hr></h2>
			</th>		
			<tr>
				<td>Email:</td>
				<td><input type="email" name="senderEmail"></td>
			</tr>
			<tr>
				<td>Subject:</td>
				<td><input type="text" autofocus="" name="senderSubject"></td>
			</tr>
			<tr>
				<td>Message:</td>
				<td><textarea name="senderMessage"></textarea></td>
			</tr>			
			<tr>
				<td></td>
				<td><input type="submit" value="SEND" name="submit"></td>
			</tr>			
			<tr>
				<td colspan="2">
					<?php 
                         session_start();
                         if (isset($_SESSION['msg'])) {
							echo $_SESSION['msg'];
                          	unset($_SESSION['msg']);
					} ?>
				</td>
					
			</tr>
		</table>
	</form>
</body>
</html>