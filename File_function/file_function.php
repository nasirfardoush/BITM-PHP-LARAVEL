<?php 
	echo "<h2>PHP file function.</h2><hr/>";
	

//basename()

	echo "<h3>* basename() * Returns trailing name component of path.</h3>";
	echo "D:\wamp\www\BITM-PHP\BITM-PHP-LARAVEL\File_function basename = ".basename("D:\wamp\www\BITM-PHP\BITM-PHP-LARAVEL\File_function");
	echo __FILE__."<br/> Basename = ".basename(__FILE__);

//dirname()
	echo  "<h3>* dirname() *Returns a parent directory's path.</h3>";
	echo dirname(__FILE__)."<br/>";
	echo 'without dirname()= '.__FILE__;

//Copy()
	 echo "<h3>* copy() * Copies file.</h3>";
	 $file = 'example.txt';
	 $newfile = 'example.txt.bak';

	 if (copy($file, $newfile)) {
	 	echo "File copied succesfully";
	 }
//fopen()
	 echo "<h3>* fopen() * Opens file or URL.</h3>";
	 $handle = fopen("example.txt","r");
	 echo $handle;
//fclose()
	 echo "<h3>* fclose() * Closes an open file pointer.</h3>";

	 echo fclose($handle);
//fgetcsv()
	echo "<h3>* fgetcsv() * Gets line from file pointer and parse for CSV fields.</h3>";

$row= 1;
if (($handle = fopen("test.csv","r")) !==FALSE) {
	while(($data = fgetcsv($handle,1000,",")) !== FALSE){
		$num  = count($data);
		echo "<p>$num fields in line $row:<br/></p>\n";

		$row++;
		for ($i=0; $i < $num; $i++) { 
			echo $data[$i]."<br/>\n";
		}
	}
	fclose($handle);
}


//file_exists()
		echo "<h3>* file_exists() * Checks whether a file or directory exists.</h3>";
		$filename = dirname(__FILE__).'/example.txt';

		echo $filename;
		if(file_exists($filename)){
			echo "The file  $filename exists";
		}else{
			echo "The file $filename does not exist";
		}
//file_put_contents
	echo "<h3>* file_put_contents() * Write a string to a file.</h3>";
	$file = 'example.txt';
	$current = file_get_contents($file);
	$current .= "I am file_get_contents. ";
	file_put_contents($file,$current);
//file_get_contents
	echo "<h3>* file_get_contents() * Reads entire file into a string.</h3>";
	$file = 'example.txt';
	$current = file_get_contents($file);
	echo $current;
//filesize
	echo "<h3>* filesize() * Gets file size.</h3>";
	$filename = __FILE__;
	echo $filename.' :'.filesize($filename).' bytes';
//filetype
	echo "<h3>* filetype() * Gets file type.</h3>";
	$filename = "example.txt";
	echo $filename.' :file type is : '.filetype($filename);
//fputcsv
	echo "<h3>* fputcsv() *Format line as CSV and write to file pointer.</h3>";
	$list = array(
		array('a','ab','abc','abcd'),
		array('1','12','123','1234'),
		array('a',"ab"),

		);
	$fp = fopen('test.csv','w');

	foreach($list as $fields){
		fputcsv($fp,$fields);

	}

	fclose($fp);

//fwrite
	echo "<h3>* fwrite() * Binary-safe file write.</h3>";
	$file = fopen("fwrite.txt","w");
	echo "Character number = ".fwrite($file,"Hllo I am fwrite !testing New value");
	fclose($file);
//is_file
	echo "<h3>* is_file() * Tells whether the filename is a regular file.</h3>";
		var_dump(is_file('example.txt'));
//unlink 
	echo "<h3>* unlink() *  Deletes a file.</h3>";
		if (is_file('test.html')) {
		$del = unlink('test.html');
		echo "File are deleted";
	}else{
		echo "File not found";
	}

