<?php 

namespace App\BITM\SEIP\student;
use PDO;
class Student{
	private $name = '';
	private $id   = '';
	private $keyword = '';

	public function __construct(){
		session_start();
	}
	//set data
	public function setData($data = ''){
		if (array_key_exists('title',$data)) {
			$this->name = $data['title'];
		}		
		if (array_key_exists('id',$data)) {
			$this->id = $data['id'];
		}		
		if (array_key_exists('keyword',$data)) {
			$this->keyword = $data['keyword'];
		}
		return $this; // For method chain

	}

	//All Student views
	public function index($noOfItemPerPage="" ,$offsetData=""){
		try{
			$pdo = new PDO('mysql:host=localhost;dbname=db_bitm','root','');
			$limit  = (!empty($noOfItemPerPage))?'LIMIT':false;
			$offset = (!empty($offsetData))?'OFFSET':false;
			$query = "SELECT SQL_CALC_FOUND_ROWS  * FROM tbl_student WHERE deleted_at='0000-00-00 00:00:00' ORDER BY ID DESC 
						 $limit $noOfItemPerPage
			  				$offset $offsetData ";

			$stmt = $pdo->prepare($query);
			$stmt->execute();
			$data = $stmt->fetchALL();

			$subquery = 'SELECT FOUND_ROWS()';
			$data['numOfRow'] = $pdo->query($subquery)->fetch(PDO::FETCH_COLUMN);
			return $data;

		}catch(PDOExecption $e){
			echo 'Error'.$e->getMessage();
		}
	}		
	// public function numberOfRow(){
	// 	try{
	// 		$pdo = new PDO('mysql:host=localhost;dbname=db_bitm','root','');
	// 		$query = "SELECT * FROM tbl_student WHERE deleted_at='0000-00-00 00:00:00'";
	// 		$stmt = $pdo->prepare($query);
	// 		$stmt->execute();
	// 		$data = $stmt->fetchALL();
	// 		return count($data);


	// 	}catch(PDOExecption $e){
	// 		echo 'Error'.$e->getMessage();
	// 	}
	// }	

	//Search 
	public function search($noOfItemPerPage="" ,$offsetData=""){
		try{
			$pdo = new PDO('mysql:host=localhost;dbname=db_bitm','root','');
			$limit  = (!empty($noOfItemPerPage))?'LIMIT':false;
			$offset = (!empty($offsetData))?'OFFSET':false;
			$query = "SELECT SQL_CALC_FOUND_ROWS  * FROM tbl_student WHERE deleted_at='0000-00-00 00:00:00' AND title LIKE '%$this->keyword%' ORDER BY ID DESC 
						 $limit $noOfItemPerPage
			  				$offset $offsetData ";

			$stmt = $pdo->prepare($query);
			$stmt->execute();
			$data = $stmt->fetchALL();

			$subquery = 'SELECT FOUND_ROWS()';
			$data['numOfRow'] = $pdo->query($subquery)->fetch(PDO::FETCH_COLUMN);
			return $data;

		}catch(PDOExecption $e){
			echo 'Error'.$e->getMessage();
		}
	}	
	public function recycleData(){
		try{
			$pdo = new PDO('mysql:host=localhost;dbname=db_bitm','root','');
			$query = "SELECT * FROM tbl_student WHERE deleted_at !='0000-00-00 00:00:00' ORDER BY ID DESC";
			$stmt = $pdo->prepare($query);
			$stmt->execute();
			$data = $stmt->fetchALL();
			return $data;

		}catch(PDOExecption $e){
			echo 'Error'.$e->getMessage();
		}
	}	

	//Student details
	public function show(){
		try{
			$pdo = new PDO('mysql:host=localhost;dbname=db_bitm','root','');
			$query = "SELECT * FROM tbl_student WHERE uniq_id='$this->id'";
			$stmt = $pdo->prepare($query);
			$stmt->execute();
			$data = $stmt->fetch();
			return $data;

		}catch(PDOExecption $e){
			echo 'Error'.$e->getMessage();
		}
	}
		public function reStore(){
			try{
				$pdo = new PDO('mysql:host=localhost;dbname=db_bitm','root','');
				$query = "UPDATE tbl_student SET deleted_at = :sDelete WHERE uniq_id=:id";
				$stmt  = $pdo->prepare($query);
				$stmt->execute(
					$r =array(
						':sDelete'=>'0000-00-00 00:00:00',
						':id' =>$this->id,
						)
				);	

					if($stmt){
						$_SESSION['msg'] = "Data Restored";
						header('Location:deleteList.php');

					}
				}catch(PDOExecption $e){
					echo 'Error'.$e->getMessage();
				}	
			}		

			public function softDelete(){
			try{
				$pdo = new PDO('mysql:host=localhost;dbname=db_bitm','root','');
				$query = "UPDATE tbl_student SET deleted_at = :sDelete WHERE uniq_id=:id";
				$stmt  = $pdo->prepare($query);
				$stmt->execute(
					$r =array(
						':sDelete'=>date('Y-m-d h:i:s'),
						':id' =>$this->id,
						)
				);
				if ($stmt) {
					
					$_SESSION['msg'] = "Deleted Successfully Deleted";
		
					header('Location:index.php');
					}
			}catch(PDOExecption $e){
				echo 'Error'.$e->getMessage();
			}
		}

		//Student delete
		public function delete(){
			try{
				$pdo = new PDO('mysql:host=localhost;dbname=db_bitm','root','');
				$query = "DELETE FROM tbl_student WHERE uniq_id='$this->id'";
				$stmt = $pdo->prepare($query);
				$stmt->execute();
				if ($stmt) {
					$_SESSION['msg'] = "Successfully Deleted";
					header('Location:index.php');
				}

			}catch(PDOExecption $e){
				echo 'Error'.$e->getMessage();
			}
		}
		//update data
		public function update(){
			try{
				$pdo = new PDO('mysql:host=localhost;dbname=db_bitm','root','');
				$query = "UPDATE tbl_student SET title = :title WHERE uniq_id=:id";
				$stmt  = $pdo->prepare($query);
				$stmt->execute(
					$r =array(
						':title'=>$this->name,
						':id' =>$this->id,
						)
				);
				if ($stmt) {
					session_start();
					$_SESSION['msg'] = "Successfully Updated";
		
					header('Location:index.php');
					}
			}catch(PDOExecption $e){
				echo 'Error'.$e->getMessage();
			}
		}


	public function Store(){
		try{
			$pdo = new PDO('mysql:host=localhost;dbname=db_bitm','root','');
			$query = "INSERT INTO tbl_student (uniq_id,title)VALUES (:uniq_id,:title)";
			$stmt  = $pdo->prepare($query);
			$stmt->execute(
					array(
						':uniq_id'=>uniqid(),
						':title'=>$this->name,
						)
				);
			if ($stmt) {
				$_SESSION['msg'] = "Successfully Submitted";
	
				header('Location:create.php');
			}
		}catch(PDOExecption $e){
			echo 'Error'.$e->getMessage();
		}
	}



}
