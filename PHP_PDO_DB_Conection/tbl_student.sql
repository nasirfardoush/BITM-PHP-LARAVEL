-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 22, 2017 at 03:41 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_bitm`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student`
--

CREATE TABLE IF NOT EXISTS `tbl_student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updeted_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  `uniq_id` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `tbl_student`
--

INSERT INTO `tbl_student` (`id`, `title`, `created_at`, `updeted_at`, `deleted_at`, `uniq_id`) VALUES
(14, 'Abdur rhaim', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Abdur rhaim587f91b94bc4d'),
(16, 'Billal hossain', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '587f92059557b'),
(17, 'Kamrul islam ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '587f920ee9c02'),
(19, 'Samsur Rhaman', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '587f944124811'),
(20, 'Abdul Karim', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '587f992679f3a'),
(21, 'Md Sabbir Rhaman', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '5884ba82808fc'),
(22, 'Huzaifa Raheb', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '5884ba9139f4c'),
(23, 'Imranul Arafat', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '5884ba9b416c4'),
(24, 'Imam Hossain', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '5884bb5113e3d'),
(25, 'Hello world How are event', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '5884bdba34e96');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
