<?php 
	include_once('../../../../vendor/autoload.php');

	use App\BITM\SEIP\student\Student;
	$obstd = new Student;
	$arr = $obstd->recycleData();

?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style>
		table{
			margin: 0px auto;
			background-color: #eef1ee;
			padding: 20px;
			min-width: 500px;
		}
		table td,table th{
			padding: 5px;
			border: 1px solid #000;
			text-align: center;
		}
	</style>
</head>
<body>
<table>
	<tr>
		<th colspan="3"><h3>Delete Student List</h3></th>
	</tr>
	<tr>
		<td colspan="3"><a href="index.php">View List</a></td>

	</tr>
	<?php
		if (isset($_SESSION['msg'])) { ?>
		<tr> <td colspan="3">
		<?php 	echo $_SESSION['msg'];
			unset($_SESSION['msg']); ?>
		</td> </tr>
		<?php } 	?>
	<tr>
		<th>ID</th>
		<th>Title</th>
		<th>Action</th>
	</tr>

<?php 


	$serial = 1;
	foreach ($arr as $key => $value) { ?>
			

	<tr>
		<td><?php echo $serial++;?></td>
		<td><?php echo  $value['title'];?></td>
		<td>
			<a href="restore.php?id=<?php echo  $value['uniq_id'];?>">Restore</a>||
			<a href="softdelete.php?id=<?php echo  $value['uniq_id'];  ?> ">Delete</a>
		</td>
	</tr>

<?php	}?>

</table>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</body>
</html>