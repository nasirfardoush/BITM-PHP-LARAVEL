<?php 
	include_once('../../../../vendor/autoload.php');
	include_once('../../../../vendor/mpdf/mpdf.php');

	use App\BITM\SEIP\student\Student;
	$obstd = new Student;
	if(isset($_GET)){
		$value = $obstd->setData($_GET)->show();

	}else{
		echo "some thing else";
		die();
	}

	$trs ="";
	$serial = 1;
		$serial++;
		$trs.="<tr>";
		$trs.="<td>".$serial."</td>";
		$trs.="<td>".$value['title']."</td>";
		$trs.="</tr>";


$html=<<<EOD

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style>
		table{
			margin: 0px auto;
			background-color: #eef1ee;
			padding: 20px;
			min-width: 500px;
		}
		table td,table th{
			padding: 5px;
			border: 1px solid #000;
			text-align: center;
		}
	</style>
</head>
<body>
<table>
	<tr>
		<th colspan="2"><h3>Student List</h3></th>
	</tr>
	<tr>
		<th>ID</th>
		<th>Title</th>
	</tr>

	<tr>
		$trs;
	</tr>
</table>

</body>
</html>
EOD;

$mpdf = new mPDF();
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;
?>

