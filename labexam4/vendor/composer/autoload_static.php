<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit2ba676b40905dc9a76f923e24a9f53aa
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit2ba676b40905dc9a76f923e24a9f53aa::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit2ba676b40905dc9a76f923e24a9f53aa::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
