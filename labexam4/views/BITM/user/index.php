<?php
include_once ('../../../vendor/autoload.php');

use App\BITM\user\User;
$objUser = new User;

$data =  $objUser->index();

?>


<!DOCTYPE html>
<html>
<head>
	<title>User Data</title>
</head>
<body>

	<h1>Answer for Q-3/4/5 <hr></h1>

	<table border="1px">
		<tr>
			<th colspan="6">All User</th>
		</tr>
		<tr>
			<th colspan="6">
				<a href="create.php">Add user</a>
			</th
		</tr>	
		
		<?php
		session_start();
		if(isset($_SESSION['msg'])){ ?>
		<tr>
			<td colspan="6">
				<?php echo "<h2>".$_SESSION['msg']."</h2>";
				unset($_SESSION['msg']); ?>
				<?php } ?>
			</td>
		</tr>

		<tr>
			<th>Serial</th>
			<th>Name</th>
			<th>Email</th>
			<th>Password</th>
			<th>Gender</th>
			<th>Action</th>
		</tr>
		<?php 
			$i = 1;
			foreach ($data as $user) { ?>
			<tr>
				<td><?php echo $i++;?></td>
				<td><?php echo $user['user_name'];?></td>
				<td><?php echo $user['email'];?></td>
				<td><?php echo $user['pasword'];?></td>
				<td><?php echo ($user['gender']==1)?'Male':'Female'; ?></td>
				<td>
					<a href="show.php?id=<?php echo $user['id'];?>">Show</a> ||
					<a href="delete.php?id=<?php echo $user['id'];?>">Delete</a> ||
					<a href="edit.php?id=<?php echo $user['id'];?>">Edit</a> ||
				</td>
			</tr>
		<?php	} ?>
	</table>



</body>
</html>