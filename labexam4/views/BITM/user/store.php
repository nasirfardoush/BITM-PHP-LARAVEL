<?php
include_once ('../../../vendor/autoload.php');

use App\BITM\user\User;
$objUser = new User;

if (!empty($_POST)) {
	if(preg_match("/([a-z])/",$_POST['user_name'])){
		$user_name = trim($_POST['user_name']);
		$user_name = htmlspecialchars($_POST['user_name']);
		$user_name = filter_var($_POST['user_name'],FILTER_SANITIZE_STRING);
		$_POST['user_name'] = $user_name;

		$objUser->setData($_POST)->store();

	}else{
		echo "Name are invlaid!!";
		die();
	}
}
