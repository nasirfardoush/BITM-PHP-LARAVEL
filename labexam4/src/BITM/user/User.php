<?php 

namespace App\BITM\user;
use PDO;

class User{

	public $user_name = '';
	public $email = '';
	public $pasword = '';
	public $gender = '';
	public $id = '';
	public $pdo = '';


	public function __construct(){
		try{
			$this->pdo = new PDO('mysql:host=localhost;dbname=db_bitm','root','');
		}catch(PDOExecption $e){
			echo 'Error'.$e->getMessage();
		}
	}
  
	//for set data
 	public function setData($data = array()){

	 	if (array_key_exists('user_name',$data)) {
	 		$this->user_name = $data['user_name'];
	 	} 	
	 	if (array_key_exists('email',$data)) {
	 		$this->email     = $data['email'];
	 	} 	
	 	if (array_key_exists('pasword',$data)) {
	 		$this->pasword   = $data['pasword'];
	 	} 	
	 	if (array_key_exists('gender',$data)) {
	 		$this->gender    = $data['gender'];
	 	} 	
	 	if (array_key_exists('id',$data)) {
	 		$this->id    = $data['id'];
	 	} 	
	 	return $this;

 	}

 	//For View All Data
 	public function index(){

 		$query = "SELECT * FROM tbl_user ORDER BY ID DESC ";
 		$con = $this->pdo->prepare($query);
 		$con->execute();
 		$data = $con->fetchALL();
 		
 		return $data;

 	} 	

 	//For finite view
 	public function show(){

 		$query = "SELECT * FROM tbl_user WHERE id= $this->id";
 		$con = $this->pdo->prepare($query);
 		$con->execute();
 		$data = $con->fetch();
 		
 		return $data;

 	} 	

 	//For finite delete
 	public function delete(){

 		$query = "DELETE FROM tbl_user WHERE id= $this->id";
 		$con = $this->pdo->prepare($query);
 		$con->execute();
 		if ($con) {
 			session_start();
 			$_SESSION['msg'] = 'Data deleted succesfully';
 			header('Location:index.php');
 		}

 	}

 	//For update finite data
 	public function update(){

 		$query = "UPDATE tbl_user SET user_name=:user_name,email=:email,pasword=:pasword,gender=:gender WHERE id=$this->id";
 		$con = $this->pdo->prepare($query);
 		$con->execute(array(
 			':user_name'=>$this->user_name,
 			':email'    =>$this->email,
 			':pasword'  =>$this->pasword,
 			':gender'   =>$this->gender


 		));
 		if ($con) {
 			session_start();
 			$_SESSION['msg'] = 'Data Updated succesfully';
 			header('Location:index.php');
 		}



 	}




 	public function store(){

 		$query = "INSERT INTO tbl_user (user_name,email,pasword,gender) VALUES(:user_name,:email,:pasword,:gender)";
 		$con = $this->pdo->prepare($query);
 		$con->execute(array(
 			':user_name'=>$this->user_name,
 			':email'    =>$this->email,
 			':pasword'  =>$this->pasword,
 			':gender'   =>$this->gender


 		));
 		if ($con) {
 			session_start();
 			$_SESSION['msg'] = 'Data inserted succesfully';
 			header('Location:create.php');
 		}



 	}




}