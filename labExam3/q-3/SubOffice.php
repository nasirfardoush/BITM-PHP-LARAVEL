<?php 

class SubOffice{
	protected $name ="Nasir";
	protected $id   ="SEIP-149613";

	protected function set($name,$id){
		$this->name = $name;
		$this->id = $id;
	}	
	protected function get(){
		$var = $this->name."<br/>".$this->id;
		return $var;
	}
}

class MainOffice extends SubOffice{
	public function info(){
		$this->set('Rahim','SEIP45456');
		echo $this->get();
	}
}

$obj = new MainOffice;

$obj->info();