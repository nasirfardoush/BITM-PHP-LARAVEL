<?php
//PHP ARITHMATIC OPERATOR
    echo "PHP ARITHMATIC OPERATOR.<br/>";
    $x = 10;
    $y = 30;
    $s = 2;
    $t =50;
    $u =20;
    $v =5;
    $z =100;
    $p = 20;
    $q =35;
    $sum =$x+$y;
    $sub =$x-$y;
    $mul =$x*$y;
    $dev =$x/$y;
    $per = $x%$y;
    $power=$s*$y*$y;

    echo "x+y =".$sum."<br/>";
    echo "x-y =".$sub."<br/>";
    echo "x*y =".$mul."<br/>";
    echo "x/y =".$dev."<br/>";
    echo "x%y =".$per."<br/>";
    echo "s**y =".$power."<br/>";

    echo "<br/>"."<br/>";

//PHP Assaingment Operator
    echo "PHP Assaingment Operator.<br/>";
    $x = $y;
    $s += $y;
    $t -= $y;
    $u *= $y;
    $v /= $y;
    $z %= $y;
    echo "x=y =".$x."<br/>";
    echo "s+=y =".$s."<br/>";
    echo "t-=y =".$t."<br/>";
    echo "u*=y =".$u."<br/>";
    echo "v/=y =".$v."<br/>";
    echo "z%=y =".$z."<br/>";
    echo "<br/>"."<br/>";
//PHP Comparison operator
    echo "PHP Comparison Operator.<br/>";
    if($p==$q){
        echo "$p==$q"." "."p==q... yes<br/>";
    }else{
        echo "$p==$q"." "."p==q... no<br/>";
    }

    if($p===$q){
        echo "$p===$q"." "."p===q... yes<br/>";
    }else{
        echo "$p===$q"." "."p===q... no<br/>";
    }

    if($p!=$q){
        echo "$p!=$q"." "."p!=q... yes<br/>";
    }else{
        echo "$p!=$q"." "."p!=q... no<br/>";
    }

    if($p<>$q){
        echo "$p<>$q"." "."p<>q... yes<br/>";
    }else{
        echo "$p<>$q"." "."p<>q... no<br/>";
    }

    if($p>$q){
        echo "$p>$q"." "."p>q... yes<br/>";
    }else{
        echo "$p>$q"." "."p>q... no<br/>";
    }

    if($p<$q){
        echo "$p<$q"." "." p<q...yes<br/>";
    }else{
        echo "$p<$q"." "."p<q... no<br/>";
    }


    if($p>$q){
        echo "$p=>$q"." "."p=>q... yes<br/>";
    }else{
        echo "$p=>$q"." "."p=>q... no<br/>";
    }

    if($p<$q){
        echo "$p<=$q"." "." p<=q...yes<br/>";
    }else{
        echo "$p<=$q"." "."p<=q... no<br/>";
    }
 echo "<br/>"."<br/>";
//PHP Increment - Decrement Operators
    echo "PHP Increment - Decrement Operators <br/>";
    $i = 1;
    echo "Defult value of i =".$i."<br/>";
    for ($i;$i<=5;$i++){
        echo "i++ =".$i."<br/>";
    };

     $j = 1;
    echo "Defult value of j =".$j."<br/>";
    for ($j;$j<=5;$j++){
        echo "++j =".$j."<br/>";
    }

    $i = 5;
    echo "Defult value of i =".$i."<br/>";
    for ($i;$i>=0;$i--){
        echo "i-- =".$i."<br/>";
    };

     $j = 5;
    echo "Defult value of j =".$j."<br/>";
    for ($j;$j>=0;--$j){
        echo "--j =".$j."<br/>";
    }    
 echo "<br/>"."<br/>";
//PHP Logical operators
    echo "PHP Logical operators <br/>";
    $data_1 ="";
    $data_2 ="String value";
    if (empty($data_1) AND !empty($data_2)) {
         echo "***The AND condition  are satisfied. <br/>";
    }else{
        echo "***The AND condition  are not satisfied! <br/>";
    }

    if (empty($data_1) OR !empty($data_2)) {
         echo "***The OR condition  are satisfied. <br/>";
    }else{
        echo "***The OR condition  are not satisfied! <br/>";
    }


    if (empty($data_1) XOR !empty($data_2)) {
         echo "***The XOR condition  are satisfied. <br/>";
    }else{
        echo "***The XOR condition  are not satisfied(Cause Every condition are true.)! <br/>";
    }    


    if (!empty($data_2)) {
         echo "***The NOT(!) condition  are satisfied. <br/>";
    }else{
        echo "***The NOT(!) condition  are not satisfied(Cause Every condition are true.)! <br/>";
    }    
