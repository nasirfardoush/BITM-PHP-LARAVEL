<?php
/*
 * Srting function
 * by MD NASIR FARDOUSH
 * */

//addslashes
    echo "** addslashes **  Quote string with slashes (Returns a string with backslashes ).<br/> ";
    $slash_test = "I 'am' bangladeshi. 'But' you?";
    echo addslashes($slash_test);

echo "<br/>"."<br/>";
//addcslashes
    echo "**addcslashes**  Quote string with slashes in a C style.<br/>";
    $cslash_test = "I am Arabian. But you?";
    echo addcslashes($cslash_test,'o');

echo "<br/>"."<br/>";
//explode
    echo "** explode ** Split a string by string.<br/> ";
    $pizza = "piece1,54545 piece2 piece3";
    $pieces = explode(" ",$pizza);
    echo "<pre/>";
    print_r ($pieces);

echo "<br/>"."<br/>";
//implode
    echo "** implode ** Join array elements with a string.<br/> ";
    $pizza = array("piece1",54545, "piece2","piece3");
    $pieces = implode(" ,",$pizza);
    echo "The implode output = ".$pieces;

echo "<br/>"."<br/>";
//.............str_split................
    echo "** str_split ** Convert a string to an array.<br/> ";
    $str = "How are you ?";
    $ar_val= str_split( $str);
    echo "String value is 'How are you ?' That's convert to array by str_split()..<pre/>";
    print_r($ar_val);

echo "<br/>"."<br/>";


//.............str_pad..............
    echo "** str_pad **  Pad a string to a certain length with another string.<br/> ";
    $str = "Add Extra value with me";
    echo str_pad($str,40,".")."<br/>";
    echo str_pad($str,40,">=",STR_PAD_LEFT)."<br/>";
    echo str_pad($str,40,">=",STR_PAD_BOTH)."<br/>";
    echo str_pad($str,50,">=",STR_PAD_BOTH)."<br/>";
    echo str_pad($str,35,"_",STR_PAD_BOTH)."<br/>";
    echo str_pad($str,35,"_",STR_PAD_RIGHT)."<br/>";
    echo str_pad($str,35,"_",STR_PAD_LEFT)."<br/>";


echo "<br/>"."<br/>";
//crypt
    echo "** crypt() ** One-way string hashing.<br/>";
    echo crypt("1tryuutyuiyrtutyft");
echo "<br/>"."<br/>";
//.............nl2br................
    echo "** nl2br() ** Inserts HTML line breaks before all newlines in a string.<br/>";
    echo nl2br("Hello \n you \n are \n bangladeshi ?");

echo "<br/>"."<br/>";
//.............str_replace................
    echo "** str_replace() ** Replace all occurrences of the search string with the replacement string.<br/>";

   //$body = str_replace("%body%","black","<body text='%body%'>");
    $vowels = array("a","e","i","o","u","A","E","I","O","U");
    $onlyconsonants = str_replace($vowels, "", "Hello World of PHP or Bangladesh");
    echo "Remomove all vowels by str_replace() output - ".$onlyconsonants;

echo "<br/>"."<br/>";
//.............str_shuffle................
    echo "** str_shuffle() ** Randomly shuffles a string.<br/>";
    $RanStr = str_shuffle('Bangladesh');
    echo "Randomly string output every refrehs -  ".$RanStr;

echo "<br/>"."<br/>";
//.............str_count................
    echo "** count() **  Count all elements in an array, or something in an object.<br/>";
    $RanStr = count(array("Bangladesh","fg", "dfgdfgh" ,"fgfdgh",45,56,67,78,89));
    echo "Count array element = ".$RanStr;

echo "<br/>"."<br/>";

//.............strlen................
    echo "** strlen() **   Get string length.<br/>";
    $Str = strlen("I am Bangladeshi and you ?");
    echo "Get string lenght = ".$Str;

echo "<br/>"."<br/>";
//.............str_word_count................
    echo "** str_word_count() ** Return information about words used in a string .<br/>";
    $Str = str_word_count("I am Bangladeshi and you ?");
    echo "Get string word count result = ".$Str;

echo "<br/>"."<br/>";
//.............strtoupper................
    echo "** strtoupper() **  Make a string uppercase.<br/>";
    $Str = strtoupper("I am Bangladeshi and you ?");
    echo "Get uppercase  string = ".$Str;

echo "<br/>"."<br/>";
//.............satrtolower................
    echo "** strtolower() **  Make a string lowercase.<br/>";
    $Str = strtolower("I am Bangladeshi and you ?");
    echo "Get lowercase  string = ".$Str;

echo "<br/>"."<br/>";

//.............ucfirst................
    echo "** ucfirst() **  Make a string's first character uppercase.<br/>";
    $Str = ucfirst("go your home");
    echo "Get first character upercase  string = ".$Str;

echo "<br/>"."<br/>";
//.............ucwords................
echo "** ucwords() **   Uppercase the first character of each word in a string.<br/>";
$Str = ucwords("go your home");
echo "Get Uppercase the first character of each word in a string = ".$Str;

echo "<br/>"."<br/>";
//crc32
    echo "<h3>The crc32() function calculates a 32-bit CRC (cyclic redundancy checksum) for a string.</h3>";
    $str = crc32("Hello world!");
    echo 'Without %u: '.$str."<br>";
    echo 'With %u: ';
    printf("%u",$str);

echo "<br/>"."<br/>";
//str_getcsv
echo "<h3>** str_getcsv ** Parse a CSV string into an array. </h3><br/>";
$csv = array_map('str_getcsv',file('data.csv'));













echo "<br/>"."<br/>"."<br/>"."<br/>"."<br/>"."<br/>";
?>