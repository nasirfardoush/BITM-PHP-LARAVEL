<?php
//boolval
    echo "**boolval**  Get the boolean \n value of a variable.<br/>";
    echo '0:    '.(boolval(0)? 'true':'false')."<br/>";
    echo '1:    '.(boolval(1)? 'true':'false')."<br/>";

    echo "<br/>";

//empty
    echo "**empty**  Determine whether a variable is empty.<br/>";
    $var = 0;
    if(empty($var)){
        echo "The variable is empty";
    }
    echo "<br/>"."<br/>";

//isset
    echo "**isset**  Determine if a variable is set and is not NULL.<br/>";
    $var = 'something';
    if (isset($var)){
        echo "Varibale exist";
    }
    echo "<br/>"."<br/>";
//gettype
    echo "**gettype** Get the type of a variable.<br/>";
    $a = 00;
    $b = .00;
    $c = 00.;
    $d = 00.00;
    $e = "e";
    $f = "$c";
    $g = '$c';
    $h = "";
    $i = "          ";
    $j = "0";
    $k = "empty";
    $l = null;
    $n = array(5,2);
    echo '$a = 00 _________type--'.gettype($a)."<br/>";
    echo '$b = .00 ________type--'.gettype($b)."<br/>";
    echo '$c = 00. ________type--'.gettype($c)."<br/>";
    echo '$d = 00.00 ______type--'.gettype($d)."<br/>";
    echo '$e = "e" ________type--'.gettype($e)."<br/>";
    echo '$f = "$c" _______type--'.gettype($f)."<br/>";
    echo '$g = \'$c\' _____type--'.gettype($g)."<br/>";
    echo '$h = "" _________type--'.gettype($h)."<br/>";
    echo '$i = "          " _____type--'.gettype($i)."<br/>";
    echo '$j = "0" _________type--'.gettype($j)."<br/>";
    echo '$k = "empty" _____type--'.gettype($k)."<br/>";
    echo '$l = null _________type--'.gettype($l)."<br/>";
    echo '$n = array(5,2) _________type--'.gettype($n)."<br/>";
    echo "<br/>"."<br/>";
//print_r
    echo "**print_r** Prints human-readable information about a variable.<br/>";
    $array_print = array ('a' => 'apple', 'b' => 'banana', 'c' => array('x', 'y','z'));
    print_r ($array_print);
    ?>
    </pre>
    <?php
    echo "<br/>"."<br/>";
//is_array
    echo "**is_array** Finds whether a variable is an array.<br/>";
    $a_val = array(5,2);
    if (is_array($a_val)){
        echo "This is array";
    }

    echo "<br/>"."<br/>";

//is_int
    echo "**is_int** Find whether the type of a variable is integer.<br/>";
    $values = array(23,'Bangladesh',30.5,35,'India');
    foreach ($values as $value){
        if (is_int($value)){
            echo "This is integer = ".$value."<br/>";
        }else{
            echo "Not integer value =".$value."<br/>";
        }
    }
    echo "<br/>"."<br/>";

//is_null
    echo  "**is_null** Finds whether a variable is NULL.<br/>";
    $val = null;
    if (is_null($val)){
        echo "The vaiable is null";
    }else{
        echo "vaiable is ".gettype($val);
    }
    echo "<br/>"."<br/>";
//serialize
    echo "** var_dump ** Dumps information about a variable.<br/>";
    $a = array(1,2,array("a","b","c"));
    var_dump($a);
    echo "<br/>"."<br/>";
//unset
    echo "**unset** Unset a given variable.<br/> ";
    function destroy_foo()
    {
        global $foo;
        unset($foo);
    }
    $foo = 'Unset Value';
    destroy_foo();
    echo $foo;
    echo "<br/>"."<br/>";

//serialize
    echo "** serialize ** Generates a storable representation of a value.<br/>";
    $single = array('orange','apple','graps fruit');
    echo '<pre>';
    print_r($single);
    $after_serialize = serialize($single);
    echo $after_serialize;
    echo "<br/>"."<br/>";
  //unserialize
    echo "** unserialize ** Creates a PHP value from a stored representation.<br/>";
    $unserializ_re = unserialize($after_serialize);
    echo '<pre>';
    print_r($unserializ_re);
    echo "<br/>"."<br/>";

//floatval
    echo "** floatval **Get float value of a variable.<br/>";
    $str = 'Bangladesh 123.456';
    $get_float_val = floatval($str);
    echo "Explode float value = ".$get_float_val."<br/>"; 

    $str = "123.456 Bangladesh ";
    $get_float_val = floatval($str);
    echo "Explode float value = ".$get_float_val."<br/>"; 
   echo "<br/>"."<br/>";

//is_bool
    echo "** is_bool **Finds out whether a variable is a boolean.<br/>";
    $ab = false;
    $ba = 0;
    if (is_bool($ab) === true){
        echo "..Varable 1...Yes, this is a boolean .<br/>";
    }else {
         echo "..Varable 1...No,is't a boolean .<br/>";
    }   

     if (is_bool($ba) === true){
        echo "..Varable 2...Yes, this is a boolean .<br/>";
    }else {
         echo "..Varable 2...No,is't a boolean .<br/>";
    }

echo "<br/>"."<br/>";
//is_object
    echo "** is_object **Finds whether a variable is an object.<br/>";
    $ab = false;
    $ba = 0;
    if (is_object($ab)){
        echo "..Varable 1...Yes, this is a object .<br/>";
    }else {
         echo "..Varable 1...No,is't a object .<br/>";
    }   
echo "<br/>"."<br/>";


//var_export()
    echo "** var_export() **Outputs or returns a parsable string representation of a variable.<br/>";
    echo "This is index array but var_export() convert this asosiative arry<br/>";
    $var_arr = array(1,2,3,array('Orange','Apple','Olive'));
    print_r(($var_arr));  
    echo "<br/>"."<br/>";







?>
