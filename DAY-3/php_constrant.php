<?php
namespace HelloBITM;
    define("variable","value");
    echo variable;
    echo "<br/>"."<br/>";
//PHP Magic constrant
//__LINE__
    echo "** __LINE__ ** Thats echo line number.<br/>";
    echo __LINE__;
    echo "<br/>"."<br/>";
//__FILE__
    echo "** __FILE__ ** Thats echo file path.<br/>";
    echo __FILE__;
    echo "<br/>"."<br/>";
//_FUNCTION_
    echo "** __FUNCTION__ ** Thats echo Function name.<br/>";
    function myname(){
        echo "my name is Nasir.<br/>";
        echo __FUNCTION__;
    }
    myname();
    echo "<br/>"."<br/>";
//__NAMESPACE__
    echo  '**__NAMESPACE__ ** ThatsReturn present name.<br/>';
    // Name  must be declare at the top
    echo __NAMESPACE__;
    echo "<br/>"."<br/>";
//__DIR__
    echo "** __DIR__ ** Thats echo directory path.<br/>";
    echo __DIR__;
    echo "<br/>"."<br/>";

