<?php 
	include_once('../../../vendor/autoload.php');

	use App\BITM\SEIP\Labexam;
	$obstd = new Labexam;

	$arr        = $obstd->setData($_GET)->search();


?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style>
		table{
			margin: 0px auto;
			background-color: #eef1ee;
			padding: 20px;
			min-width: 500px;
		}
		table td,table th{
			padding: 5px;
			border: 1px solid #000;
			text-align: center;
		}
		.pagination {
		  margin: 0px auto;
		  text-align: center;
		  padding: 5px;
		}
		.pagination > a, button {
		  color: #205b10;
		  font-size: 20px;
		  font-weight: bold;
		  text-decoration: none;
		}
	</style>
</head>
<body>
<table>
	<tr>
		<th colspan="1"><h3>Dowanload as <a href="pdf.php?keyword=<?php echo $_GET['keyword'];?>">PDF</a></h3></th>
	</tr>	
	<tr>
		<th colspan="2"><h3>Student List</h3></th>
		<th colspan="1">
			<form action="index.php" method="POST">
			<select name="limit">
				<option>5</option>
				<option>10</option>
				<option>15</option>
			</select>
			<input type="submit" value="Set Limit" name="submit">
			</form>
		</th>
	</tr>
	<tr>
		<td colspan="3">

			<!-- Search Option -->
			<form action="" method="GET">
				<input type="search" name="keyword">
				<input type="submit" value="Search" name="submit">
			</form>


		</td>
	</tr>
	<tr>
		<td colspan="2"><a href="index.php">All Student</a></td>
		<td colspan="1"><a href="deleteList.php">Recycle Bin</a></td>
	</tr>
	<?php
		if (isset($_SESSION['msg'])) { ?>
		<tr> <td colspan="3">
		<?php 	echo $_SESSION['msg'];
			unset($_SESSION['msg']); ?>
		</td> </tr>
		<?php } 	?>
	<tr>
		<th>ID</th>
		<th>Title</th>
		<th>Hobies</th>
		<th>Action</th>
	</tr>

<?php 


	$serial = 1;
	foreach ($arr as $key => $value) { ?>
			

	<tr>
		<td><?php echo $serial++;?></td>
		<td><?php echo  $value['title'];?></td>
		<td><?php echo  $value['hobiesname'];?></td>
		<td>
			<a href="show.php?id=<?php echo  $value['id'];?>">View Details</a>||
			<a href="delete.php?id=<?php echo  $value['id'];  ?> ">Delete</a>
		</td>
	</tr>

<?php	}?>

</table>


 </section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</body>
</html>