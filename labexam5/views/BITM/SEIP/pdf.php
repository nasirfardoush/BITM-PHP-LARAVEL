<?php 
	include_once('../../../vendor/autoload.php');
	include_once('../../../vendor/mpdf/mpdf.php');

	use App\BITM\SEIP\Labexam;
	$obstd = new Labexam;

	if(isset($_GET['pageNo'])){
		if (isset($_POST['limit'])) {
		
			$noOfItemPerPage = $_POST['limit'];
		}else{
			$noOfItemPerPage =5;
	}

	if (isset($_GET['pageNo'])) {
			$noOfPage        = $_GET['pageNo']-1;
		}else{
			$noOfPage        = 0;
		}

	$offsetData = $noOfItemPerPage * $noOfPage;
	if ($offsetData == 0) {
		$offsetData = NULL;
	}
	$arr        = $obstd->index($noOfItemPerPage ,$offsetData);
	$numberOfRow =$arr['numOfRow'];

	
	if ($numberOfRow > $noOfItemPerPage) {
		$numberOfPage    = ceil($numberOfRow / $noOfItemPerPage);
	}else{
		$numberOfPage =NULL;
	}
	array_pop($arr);

		$trs ="";
		$serial = 1;
		$serial += $offsetData;
		foreach ($arr as $value) { 		
			$serial++;
			$trs.="<tr>";
			$trs.="<td>".$serial."</td>";
			$trs.="<td>".$value['title']."</td>";
			$trs.="</tr>";
		}

	}	
	if(isset($_GET['keyword'])){
		

		$arr   = $obstd->setData($_GET)->search();
		$trs ="";
		$serial = 1;
		$serial += $offsetData;
		foreach ($arr as $value) { 		
			$serial++;
			$trs.="<tr>";
			$trs.="<td>".$serial."</td>";
			$trs.="<td>".$value['title']."</td>";
			$trs.="</tr>";
		}

	}elseif(isset($_GET['id'])){
		$value = $obstd->setData($_GET)->show();
		$trs ="";
		$serial = 1;
		$trs.="<tr>";
		$trs.="<td>".$serial."</td>";
		$trs.="<td>".$value['title']."</td>";
		$trs.="</tr>";

	}

$html=<<<EOD

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style>
		table{
			margin: 0px auto;
			background-color: #eef1ee;
			padding: 20px;
			min-width: 500px;
		}
		table td,table th{
			padding: 5px;
			border: 1px solid #000;
			text-align: center;
		}
	</style>
</head>
<body>
<table>
	<tr>
		<th colspan="2"><h3>Dowanload search result</h3></th>
	</tr>
	<tr>
		<th>ID</th>
		<th>Title</th>
	</tr>

	<tr>
		$trs;
	</tr>
</table>

</body>
</html>
EOD;

$mpdf = new mPDF();
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;
?>

