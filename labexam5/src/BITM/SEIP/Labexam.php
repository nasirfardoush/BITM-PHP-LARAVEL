<?php 

namespace App\BITM\SEIP;
use PDO;
class Labexam extends PDO{
		private $dsn = 'mysql:host=localhost;dbname=db_bitm';
		private $dbuser = 'root';
		private $dbpass = '';


	private $title = '';
	private $hobies = '';
	private $hobiesName = '';
	private $id   = '';
	private $keyword = '';

	public function __construct(){
		try{
			parent::__construct($this->dsn, $this->dbuser, $this->dbpass);

		}catch(PDOExecption $e){
			echo 'Error'.$e->getMessage();
		}
	}
	//set data
	public function setData($data = ''){
		if (array_key_exists('title',$data)) {
			$this->title = $data['title'];
		}		
		if (array_key_exists('hobies',$data)){
			$this->hobies = implode('',$data['hobies']);
				$hobieName = str_split($this->hobies );
				$hobieName = array_flip($hobieName);
				$hobiesNa='';

			foreach($hobieName as $key=>$val){

					if($key == 1){
						$hobiesNa .= "Gaming ";
					}elseif($key == 2){
						$hobiesNa .= "Fishing ";
					}elseif($key == 3){
						$hobiesNa .= "Reading ";
					}elseif($key == 4){
						$hobiesNa.= "Sleeping ";
					}elseif($key == 5){
						$hobiesNa .= "Drawing ";
					}elseif($key == 6){
						$hobiesNa .= "Working ";
					}elseif($key == 7){
						$hobiesNa .= "Singing ";
					}
					$this->hobiesName = $hobiesNa;
				}	
		}	
		if (array_key_exists('id',$data)) {
			$this->id = $data['id'];
		}		
		if (array_key_exists('keyword',$data)) {
			$this->keyword = $data['keyword'];
		}
		return $this; // For method chain

	}

	public function show(){
			$query = "SELECT * FROM tbl_labexam WHERE deleted_at='0000-00-00 00:00:00' AND id=$this->id";
			$stmt  = $this->prepare($query);
			$stmt->execute();
			$data = $stmt->fetch();
			return $data;

	}	

	public function index($noOfItemPerPage="" ,$offsetData=""){
			$limit  = (!empty($noOfItemPerPage))?'LIMIT':false;
			$offset = (!empty($offsetData))?'OFFSET':false;
			$query = "SELECT SQL_CALC_FOUND_ROWS  * FROM tbl_labexam WHERE deleted_at='0000-00-00 00:00:00' ORDER BY ID DESC 
						 $limit $noOfItemPerPage
			  				$offset $offsetData ";

			$stmt  = $this->prepare($query);
			$stmt->execute();
			$data = $stmt->fetchALL();

			$subquery = 'SELECT FOUND_ROWS()';
			$data['numOfRow'] = $this->query($subquery)->fetch(PDO::FETCH_COLUMN);

			return $data;
		}
	
		public function search(){

			$query = "SELECT   * FROM tbl_labexam WHERE hobiesname LIKE '%$this->keyword%' ";

			$stmt  = $this->prepare($query);
			$stmt->execute();
			$data = $stmt->fetchALL();
			return $data;
		}
	
	

	public function store(){
			$query = "INSERT INTO tbl_labexam (title,hobies,hobiesname)VALUES (:title,:hobies,:hobiesname)";
			$stmt  = $this->prepare($query);
			$stmt->execute(
					array(
						':title'=>$this->title,
						':hobies'=>$this->hobies,
						':hobiesname'=>$this->hobiesName,
						)
				);
			if ($stmt) {
				session_start();
				$_SESSION['msg'] = "Successfully Submitted";
	
				header('Location:create.php');
			}	
		}	
		public function update(){
			$query = "UPDATE tbl_labexam SET title='$this->title' , hobies='$this->hobies'  WHERE id = $this->id";
			$stmt  = $this->prepare($query);
			$stmt->execute();
			if ($stmt) {
				session_start();
				$_SESSION['msg'] = "Successfully Updated";
	
				header('Location:index.php');
			}	
		}
	public function delete(){

			$query = "DELETE FROM tbl_labexam WHERE id='$this->id'";
			$stmt = $this->prepare($query);
			$stmt->execute();
			if ($stmt) {
				session_start();
				$_SESSION['msg'] = "Successfully Deleted";
				header('Location:index.php');
			}
		}

}
