<?php 
	include_once('../../../vendor/autoload.php');

	use App\BITM\SEIP\Labexam;
	$obstd = new Labexam;

if (!empty($_POST['title'])) {
	if (preg_match("/([a-z])/",$_POST['title'])) {
		$_POST['title'] = filter_var($_POST['title'],FILTER_SANITIZE_STRING);
		$obstd->setData($_POST)->store();
	}else{
		$_SESSION['msg'] = "Invalid Charecter !!! ";
		header('Location:create.php');
	}
}else{
	$_SESSION['msg'] = "Input can't be empty";
	header('Location:create.php');
}

