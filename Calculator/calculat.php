<?php
include "result.php";
?>
<DOCTYPE html>
    <head>
        <title>Form for Test GET and POST</title>
        <style>
            table{
                width :350px;
                background:#ddd ;
                padding:20px;
            }
        </style>
    </head>
    <body>
    <form method="POST" action="">
        <table>
            <tr>
                <td>Your result =</td>
                <td>
                    <input  value ="<?php if(isset($get_result)){ echo $get_result;}?>" />
                </td>
            </tr>
            <tr>
                <td> Number 1 : </td>
                <td> <input type="text"  name="num1" /></td>
            </tr>
            <tr><td colspan="2"><?php if(isset($emp_msg1)){ echo  $emp_msg1 ;   } ?></td> </tr>
            <tr>
                <td> Number 2 : </td>
                <td> <input type="text"  name="num2" />  </td>
            </tr>
            <tr><td colspan="2"><?php if(isset($emp_msg2)){ echo  $emp_msg2 ;   } ?></td> </tr>
            <tr>
                <td colspan="2"><input type="submit" value="ADD" name="submit" />
                <input type="submit" value="SUB" name="submit" />
                <input type="submit" value="MUL" name="submit" />
                <input type="submit" value="DIV" name="submit" /></td>
            </tr>
        </table>

    </form>
    </body>
</DOCTYPE>