-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 28, 2017 at 07:53 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_bitm`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_labexam`
--

CREATE TABLE `tbl_labexam` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `hobies` varchar(255) NOT NULL,
  `hobiesname` varchar(255) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_labexam`
--

INSERT INTO `tbl_labexam` (`id`, `title`, `hobies`, `hobiesname`, `updated_at`, `created_at`, `deleted_at`) VALUES
(16, 'Abdul Kadir', '136', 'Gaming Reading Working ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'Karim', '4', 'Sleeping ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'Imran', '5', 'Drawing ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'Sabbir vai', '36', 'Reading Working ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'Rakib vai', '35', 'Reading Drawing ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_registraed_user`
--

CREATE TABLE `tbl_registraed_user` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_password` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  `iuo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_registraed_user`
--

INSERT INTO `tbl_registraed_user` (`user_id`, `user_name`, `user_email`, `user_password`, `created_at`, `updated_at`, `deleted_at`, `iuo`) VALUES
(1, 'Nasir', 'nasir@gmail.com', '123', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(2, 'MD Ryhan', 'ryhan@gmail.com', 'ÇM—°®%~Dª[­é{¯', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(3, 'Imranul Arafat Topu', 'topu@gmail.com', 'ÇM—°®%~Dª[­é{¯', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(4, 'nasir123', 'topu@gmail.com', 'ÇM—°®%~Dª[­é{¯', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(5, 'nasir123', 'topu@gmail.com', 'ÇM—°®%~Dª[­é{¯', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(6, 'nf22', 'nf@gmail.com', 'Ü›ÛRÐMÂ\06ÛØ1>ÐU', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student`
--

CREATE TABLE `tbl_student` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updeted_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  `uniq_id` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_student`
--

INSERT INTO `tbl_student` (`id`, `title`, `created_at`, `updeted_at`, `deleted_at`, `uniq_id`) VALUES
(14, 'Abdur rhaim', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Abdur rhaim587f91b94bc4d'),
(16, 'Billal hossain', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '587f92059557b'),
(17, 'Kamrul islam ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '587f920ee9c02'),
(19, 'Samsur Rhaman', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '587f944124811'),
(20, 'Abdul Karim', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '587f992679f3a'),
(21, 'Md Sabbir Rhaman', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '5884ba82808fc'),
(22, 'Huzaifa Raheb', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '5884ba9139f4c'),
(23, 'Imranul Arafat', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '5884ba9b416c4'),
(24, 'Imam Hossain', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '5884bb5113e3d'),
(25, 'Hello world How are event', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-01-23 04:13:50', '5884bdba34e96'),
(26, 'Karim', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '58859893f21f0'),
(27, 'Abdul Kadir', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '58859b79d484a'),
(28, 'hhhhhh', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '58859b7f41d02'),
(29, 'Rahim', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '58859b84cc63c'),
(30, 'Babu', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '58859b8c4fbfb'),
(31, 'Imran', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '58859bbda620a'),
(32, 'Rahim', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '58859bc674652'),
(33, 'Torikul', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '58859bcc56619'),
(34, 'Rahim', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '58882436497c8');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `email` varchar(150) NOT NULL,
  `pasword` varchar(100) NOT NULL,
  `gender` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `user_name`, `email`, `pasword`, `gender`) VALUES
(23, 'pasir', 'abdullahcmt2@gmail.com', '1235454', '1'),
(24, 'Topu', 'topu@gmail.com', '564545', '2'),
(25, 'Fatema Begum', 'fa@hotmali.com', 'wertt', '2');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_labexam`
--
ALTER TABLE `tbl_labexam`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_registraed_user`
--
ALTER TABLE `tbl_registraed_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `tbl_student`
--
ALTER TABLE `tbl_student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_labexam`
--
ALTER TABLE `tbl_labexam`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `tbl_registraed_user`
--
ALTER TABLE `tbl_registraed_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_student`
--
ALTER TABLE `tbl_student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
