<?php
 
 class  Subject{
 	// public $name = "English";
 	// public $code = "E4086";
 	public function __construct(){
 		echo "Inside class.<br/>".$this->getDetails('Bangla','B223');
 	}
 	public function getDetails($name,$code){

 		$info = "Subject name:$name<br/>Code:$code<br/>";
 		return $info;
 	}

 }

 $obj = new Subject;

echo "Outside class.<br/>".$obj->getDetails('Eng','E5454')."<br/>";


class Mark extends Subject{

	public function __construct(){
		echo "Child class.<br/>".$this->getDetails('Math','M5454')."Mark :87";
	}
}
new Mark;

class Out{
	public function __construct(){
		$obj = new Subject;
		echo "Call to outer class".$obj->getDetails('Out','Class');
	}
}
new Out;
