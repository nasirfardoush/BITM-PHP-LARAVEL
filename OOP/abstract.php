<?php 
/**
* Abstract is empty class
* Only method define into this 
* method have/haven't boady part
* work by extends to another class
* And not possible to multiple abstract extends in a class.
* directly object create are not posible for abstract class
**/

//Abstract class

abstract class Subject{

	//non abstract method
	public function mySubject(){
		echo " Chemestry <br/>";
	}

	//abstract method
	abstract public function myFruit();

}


class Student extends Subject{

	public function __construct(){
		echo $this->mySubject();
	}

	public function mySubject(){
		return parent::mySubject()."Sorry ! My favourite subject is Physics<br/>";
	}
	public function myFruit(){
		return "My favourite fruit is Orange<br/>";
	}
}


$obj = new Student;

echo $obj->myFruit();
